<?php

require('vendor/zendframework/zend-server/src/Client.php');
require('vendor/zendframework/zend-soap/src/Client.php');
require('vendor/zendframework/zend-soap/src/Client/Common.php');

//$wsdlUrl = 'http://samplem2.local/soap/V1/customers/?wsdl&services=customerAccountManagementV1,customerCustomerRepositoryV1';
$wsdlUrl = 'http://samplem2.local/soap/V1/customers/1';
$token = '123456';

$opts = ['http' => ['header' => "Authorization: Bearer " . $token]];
$context = stream_context_create($opts);

$soapClient = new Zend\Soap\Client($wsdlUrl);
$soapClient->setSoapVersion(SOAP_1_2);
$soapClient->setStreamContext($context);

$result = $soapClient->customerAccountManagementV1IsReadonly(array('customerId' => 1));
var_dump($result);
