<?php

namespace Unit6\ComputerGames\Ui\Component\Form;

class TrialPeriods implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {

        $options = array(
            array(
                'label' => 'No',
                'value' => '0'
            ),
            array(
                'label' => 'Month',
                'value' => '1'
            ),
            array(
                'label' => '2 Months',
                'value' => '2'
            ),
            array(
                'label' => '6 Months',
                'value' => '6'
            ),
            array(
                'label' => 'Year',
                'value' => '12'
            )
        );

        return $options;
    }
}
