<?php

namespace Training\Api\Model;

class Hello implements \Training\Api\Api\Data\HelloInterface
{
    public function sayHello()
    {
        return "HELLO WORLD!";
    }
}
