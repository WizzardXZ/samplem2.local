<?php

namespace Training\Vendor\Model;

/**
 * Vendor Model
 *
 * @method \Training\Vendor\Model\Orm _getResource()
 * @method \Training\Vendor\Model\Resource\Orm getResource()
 */
class Vendor extends \Magento\Framework\Model\AbstractModel
{
    protected function _constructor()
    {
        $this->_init('Training\Vendor\Model\Resource\Orm');
    }
}
