<?php

namespace Training\Orm\Setup;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Store\Model\StoreManager;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Orm setup factory
     *
     * @var OrmSetupFactory
     */
    private $ormSetupFactory;

    protected $catalogSetupFactory;
    protected $customerSetupFactory;
    protected $storeManager;

    /**
     * Init
     *
     * @param OrmSetupFactory $taxSetupFactory
     * @param CategorySetupFactory $categorySetupFactory
     * @param CustomerSetupFactory $customerSetupFactory
     * @param StoreManager $storeManager
     */
    public function __construct(
        OrmSetupFactory $ormSetupFactory,
        CategorySetupFactory $categorySetupFactory,
        CustomerSetupFactory $customerSetupFactory,
        StoreManager $storeManager
    )
    {
        $this->ormSetupFactory = $ormSetupFactory;

        $this->catalogSetupFactory = $categorySetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.2') < 0) {
            /** @var OrmSetup $ormSetup */
            $ormSetup = $this->ormSetupFactory->create(['setup' => $setup]);

            /**
             * Add example_multiselect attribute to the 'eav_attribute' table
             */
            $catalogInstaller = $ormSetup->getCatalogSetup(['resourceName' => 'catalog_setup']);
            $catalogInstaller->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'example_multiselect',
                [
                    'name'             => 'Ex Multiselect Attribute',
                    'group'            => 'Product Details',
                    'required'         => false,
                    'input'            => 'multiselect',
                    'label'            => 'Example Multiselect',
                    'visible'          => true,
                    'visible_on_front' => true,
                    'global'           => ScopedAttributeInterface::SCOPE_STORE
                ]
            );
        }

        if (version_compare($context->getVersion(), '0.0.3') < 0) {
            /** @var OrmSetup $ormSetup */
            $ormSetup = $this->ormSetupFactory->create(['setup' => $setup]);
            $catalogInstaller = $ormSetup->getCatalogSetup(['resourceName' => 'catalog_setup']);
            $catalogInstaller->updateAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'example_multiselect',
                'backend_model',
                'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend'
            );
        }

        if (version_compare($context->getVersion(), '0.0.4') < 0) {
            /** @var OrmSetup $ormSetup */
            $ormSetup = $this->ormSetupFactory->create(['setup' => $setup]);
            $catalogInstaller = $ormSetup->getCatalogSetup(['resourceName' => 'catalog_setup']);
            $catalogInstaller->updateAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'example_multiselect',
                [
                    'frontend_model'           => \Training\Orm\Entity\Attribute\Frontend\HtmlList::class,
                    'is_html_allowed_on_front' => 1,
                ]
            );
        }

        if (version_compare($context->getVersion(), '0.0.5') < 0) {
            /** @var CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerSetup->addAttribute(
                \Magento\Customer\Model\Customer::ENTITY,
                'priority',
                [
                    'label'    => 'Priority',
                    'type'     => 'int',
                    'input'    => 'select',
                    'source'   => \Training\Orm\Entity\Attribute\Source\CustomerPriority::class,
                    'required' => 0,
                    'system'   => 0,
                    'position' => 100
                ]
            );
            $customerSetup->getEavConfig()->getAttribute('customer', 'priority')
                ->setData('used_in_forms', ['adminhtml_customer'])
                ->save();
        }
    }
}
