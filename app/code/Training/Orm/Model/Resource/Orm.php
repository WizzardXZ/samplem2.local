<?php

namespace Training\Vendor\Model\Resource;


class Orm extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init(
            'training_orm_entity',
            'orm_id'
        );
    }
}
