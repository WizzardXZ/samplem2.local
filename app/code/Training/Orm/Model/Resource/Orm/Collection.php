<?php

namespace Training\Vendor\Model\Resource\Orm;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Training\Orm\Model\Orm',
            'Training\Orm\Model\Resource\Orm'
        );
    }
}
