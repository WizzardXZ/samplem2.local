<?php

namespace Training\Vendor\Model;

/**
 * Vendor Model
 *
 * @method \Training\Vendor\Model\Orm _getResource()
 * @method \Training\Vendor\Model\Resource\Orm getResource()
 */
class Orm extends \Magento\Framework\Model\AbstractModel
{
    protected function _constructor()
    {
        $this->_init('Training\Orm\Model\Resource\Orm');
    }
}
