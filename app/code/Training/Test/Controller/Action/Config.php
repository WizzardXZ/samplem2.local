<?php

namespace Training\Test\Controller\Action;

class Config extends \Magento\Framework\App\Action\Action
{
    /*Hit a page /test/action/config. You will see:
        HELLO
        HELLO 2
    */
    public function execute()
    {
        $testConfig = $this->_objectManager->get('Training\Test\Model\Config\ConfigInterface');

        $myNodeInfo = $testConfig->getMyNodeInfo();

        if (is_array($myNodeInfo)) {
            foreach ($myNodeInfo as $str) {
                $this->getResponse()->appendBody($str . "<BR>");
            }
        }
    }
}
