<?php

namespace Training\Test\Controller\Action;

class Index extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
//        $this->getResponse()->appendBody("HELLO WORLD");

//        $this->_redirect('catalog/category/view/id/38');

        $block = $this->_view->getLayout()->createBlock('Training\Test\Block\Test');
        $block->setTemplate('test.phtml');
        $this->getResponse()->appendBody($block->toHtml());
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @return bool
     */
/*    protected function _isAllowed() {
        $secret = $this->getRequest()->getParam('secret');
        return isset($secret) && (int)$secret==1;
    }*/
}
