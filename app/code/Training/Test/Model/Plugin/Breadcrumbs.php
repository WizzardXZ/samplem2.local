<?php

namespace Training\Test\Model\Plugin;

class Breadcrumbs
{
    protected $_crumbs;

    public function aroundaddCrumb(\Magento\Theme\Block\Html\Breadcrumbs $subject, callable $proceed, $crumbName, $crumbInfo)
    {
            $crumbInfo['label'] .= "(!)";
            $this->_crumbs[$crumbName] = $crumbInfo;
//        var_dump($crumbInfo);

        return $this;
    }
}
