<?php

namespace Training\Test\Model\Plugin;

class Footer
{
    public function aftergetCopyright(\Magento\Theme\Block\Html\Footer $subject)
    {
        return 'Customized copyright!';
    }
}
