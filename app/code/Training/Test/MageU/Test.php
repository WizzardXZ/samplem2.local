<?php

namespace Training\Test\MageU;

class Test
{
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Checkout\Model\Session $session,
        \Training\Test\Api\ProductRepositoryInterface $testProductRepository,
        $justAParameter = false,
        array $data
    )
    {
    }
}
